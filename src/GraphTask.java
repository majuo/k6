import java.util.*;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

   /** Main method. */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }

   /** Actual main method to run examples and everything. */
   public void run() {
      //test 1
      Graph graph_1 = new Graph ("GRAPH_1");
      graph_1.createRandomSimpleGraph (1, 0);
      System.out.println (graph_1);
      System.out.println("-----------MST--------------");
      System.out.println(graph_1.findMST());
      System.out.println("============================");


      //test 2
      Graph graph_2 = new Graph ("GRAPH_2");
      graph_2.createRandomSimpleGraph (4, 3);
      System.out.println (graph_2);
      System.out.println("-----------MST--------------");
      System.out.println(graph_2.findMST());
      System.out.println("============================");


      //test 3
      Graph graph_3 = new Graph ("GRAPH_3");
      graph_3.createRandomSimpleGraph (4, 4);
      System.out.println (graph_3);
      System.out.println("-----------MST--------------");
      System.out.println(graph_3.findMST());
      System.out.println("============================");


      //test 4
      Graph graph_4 = new Graph ("GRAPH_4");
      graph_4.createRandomSimpleGraph (5, 10);
      System.out.println (graph_4);
      System.out.println("-----------MST--------------");
      System.out.println(graph_4.findMST());
      System.out.println("============================");


      //test 5
      Graph graph_5 = new Graph ("GRAPH_5");
      graph_5.createRandomSimpleGraph (10, 15);
      System.out.println (graph_5);
      System.out.println("-----------MST--------------");
      System.out.println(graph_5.findMST());
      System.out.println("============================");


      //test exception 1
      try {
         Graph graph_6 = new Graph("GRAPH_6");
         graph_6.createRandomSimpleGraph(5, 3);
         System.out.println(graph_6);
         System.out.println("-----------MST--------------");
         System.out.println(graph_6.findMST());
         System.out.println("============================");
      } catch (IllegalArgumentException e) {
         System.out.println("IllegalArgument exception caught. Message: " + e.getMessage());
      }


      //test exception 2
      try {
         Graph graph_7 = new Graph("GRAPH_7");
         graph_7.createRandomSimpleGraph(4, 7);
         System.out.println(graph_7);
         System.out.println("-----------MST--------------");
         System.out.println(graph_7.findMST());
         System.out.println("============================");
      } catch (IllegalArgumentException e) {
         System.out.println("IllegalArgument exception caught. Message: " + e.getMessage());
      }


      //test 5
      Graph graph_2k = new Graph ("GRAPH_2k_VERTICES");
      graph_2k.createRandomSimpleGraph (2000, 2500);
      System.out.println (graph_2k);
      System.out.println("-----------MST--------------");
      long time1 = System.currentTimeMillis();
      System.out.println(graph_2k.findMST());
      long time2 = System.currentTimeMillis();
      System.out.println("MST was found in " + (float)(time2 - time1) / 1000 + " seconds");
      System.out.println("============================");
   }

   /**Object for vertex in graph.
    * This program is designed to create simple random graphs that consist of
    * vertices and arcs(author: Jaanus Pöial, PhD) and find the minimum spanning
    * tree of the graphs using Prim's(Jarnik's) algorithm.
    */
   class Vertex {

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;
      // You can add more fields, if needed

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

   }


   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      private int info = 0;
      private int weight;
      // You can add more fields, if needed

      Arc (String s, Vertex v, Arc a, int w) {
         id = s;
         target = v;
         next = a;
         weight = w;
      }

      Arc (String s) {
         this (s, null, null, 1);
      }

      public int getWeight() {
         return weight;
      }

      @Override
      public String toString() {
         return id + " (w: " + weight + ")";
      }

   } 


   class Graph {

      private String id;
      private Vertex first;
      private int info = 0;

      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph (String s) {
         this (s, null);
      }

      @Override
      public String toString()
      {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append (" ");
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.target.toString());
               sb.append (")  |  ");
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }

      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         return res;
      }

      public Arc createArc (String aid, Vertex from, Vertex to, int weight) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         res.weight = weight;
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               Random rand = new Random();
               int arcWeight = rand.nextInt(9) + 1;
               createArc ("a" + varray [vnr].toString() + "_"
                  + varray [i].toString(), varray [vnr], varray [i], arcWeight);
               createArc ("a" + varray [i].toString() + "_"
                  + varray [vnr].toString(), varray [i], varray [vnr], arcWeight);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException 
               ("Impossible number of edges: " + m);
         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges

         Random rand = new Random();
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j) {
               continue;  // no loops
            }
            if (connected [i][j] != 0 || connected [j][i] != 0) {
               continue;  // no multiple edges
            }
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            int arcWeight = rand.nextInt(9) + 1;
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj, arcWeight);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi, arcWeight);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }


      /**
       * Find Minimum Spanning Tree(MST) graph.
       * @return Graph This returns the MST of the graph.
       */
      public Graph findMST() {
         ArrayList<Vertex> Q = getQ();
         Graph mst = new Graph(this.id + "_MST", Q.get(0));
         Q.remove(0);

         while (Q.size() > 0) {
            findAndAddClosestVertex(mst, Q);
         }
         createAllReverseArcs(mst);
         return mst;
      }

      /**
       * Find in list of not connected vertices(q) the one is 'the closest' to the MST(has minnimum weight)
       * @param mst Minimum Spanning Tree that is being constructed
       * @param q queue - list of elements that have to be added to the MST
       */
      public void findAndAddClosestVertex(Graph mst, ArrayList<Vertex> q) {
         HashMap<Arc, Vertex> pairs = getAllPairs(q);

         Arc min = findArcWithMinWeight(pairs);

         Vertex v_MST = mst.first;
         while (!v_MST.id.equals(pairs.get(min).id)) {
            v_MST = v_MST.next;
         }

         for (Vertex vert: q) {
            if (vert.id.equals(min.target.id)) {
               min.target = vert;
               break;
            }
         }
         createArc("a" + v_MST.id + "_" + min.target.id, v_MST, min.target, min.weight);
         for (Vertex vertex: q) {
            if (vertex.id.equals(min.target.id)){
               q.remove(vertex);
               break;
            }
         }
      }

      /**
       *
       * @param pairs HashMap with pairs of arcs and vertices. Contains pairs with all unvisited vertices(that are in Q).
       *              Contains only 1 pair with each vertex. Arc here is the arc with minimum weight that is incident to the vertex
       *              and suitable for adding into MST(which means that it won't create loops and the vertice where this arc
       *              starts is part of MST).
       * @return Arc  Return arc from the dictionary that has minimum weight.
       */
      public Arc findArcWithMinWeight(HashMap<Arc, Vertex> pairs) {
         Arc min = null;
         for (Arc a: pairs.keySet()) {
            if (min == null) {
               min = a;
            }
            if (a.weight < min.weight) {
               min = a;
            }
         }
         return min;
      }

      /**
       * Loop through all vertices and arcs that the vertices have and return HashMap where keys are vertices and
       * values - arcs with minimum weight. 1 arc per each vertice.
       * @param q List of elements that haven't been added to MST yet.
       * @return  HashMap<Arc, Vertex> Return HashMap with pairs of arcs and vertices, 1 pair per each vertice that is in q.
       *          Arc that is added to HashMap is incident to the vertex it is added with, also it has minimum weight among
       *          the other arcs that are incident to the vertex. Arc is also suitable for adding into MST(arc's start-vertex
       *          is part of MST already and arc's target is in q - not part of MST).
       */
      public HashMap<Arc, Vertex> getAllPairs(ArrayList<Vertex> q){
         HashMap<Arc, Vertex> pairs =new HashMap<>(); //pair of vertex and arc of that vertex with minimum weight
         Vertex v = this.first;
         while (v != null) {
            Arc a = v.first;
            ArrayList<Arc> suitableArcs = getAllSuitableArcs(a, q, v);
            if (suitableArcs.size() == 0) {
               v = v.next;
               continue;
            }
            Arc min = suitableArcs.get(0) ;
            for (Arc element: suitableArcs) {
               element = new Arc(element.id, element.target, null, element.weight);
               if (element.weight <= min.weight) {
                  min = element;
               }
            }
            pairs.put(min, v);
            v = v.next;
         }
         return pairs;
      }

      /**
       * Get all arcs incident to vertex that can be added to MST. Arc can be added if its starting point is part of MST and
       * the target isn't.
       * @param q List of elements that haven't been added to MST yet.
       * @param a First arc of the vertex.
       * @param v Vertex for which arc are checked and added to list.
       * @return ArrayList<Arc> List of arcs suitable for adding to MST.
       */
      public ArrayList<Arc> getAllSuitableArcs(Arc a, ArrayList<Vertex> q, Vertex v) {
         ArrayList<Arc> suitableArcs = new ArrayList<>();
         while (a != null) {
            if (qContains(q, v) || !qContains(q, a.target)) {
               a = a.next;
               continue;
            }
            suitableArcs.add(a);
            a = a.next;
         }
         return suitableArcs;
      }

      /**
       * Loop through all arcs that MST has and create reverse arcs to make MST an undirected graph.
       * @param mst Minimum Spanning Tree that is being constructed.
       */
      public void createAllReverseArcs(Graph mst){
         HashMap<Arc, Vertex> allArcs = new HashMap<>(); // key - arc, value - vertex
         Vertex v = mst.first;
         while (v != null) {
            Arc a = v.first;
            while (a != null) {
               allArcs.put(a, v);
               a = a.next;
            }
            v = v.next;
         }
         for (Arc each: allArcs.keySet()) {
            createArc("a" + each.target.id + "_" + allArcs.get(each).id, each.target, allArcs.get(each), each.weight);
         }
      }

      /**
       * Check if the q has vertex with same id as given vertex. It is needed since objects that were added to the q aren't
       * the same objects that are in original graph so comparing is done using id.
       * @param q List of vertices that haven't been added to MST yet.
       * @param vertex Vertex to compare. Its id will be compared to all vertices' ids from q.
       * @return true if vertex with the same id was found in q, otherwise return false.
       */
      public boolean qContains(ArrayList<Vertex> q, Vertex vertex) {
         for (Vertex element: q) {
            if (element.id.equals(vertex.id)) {
               return true;
            }
         }
         return false;
      }


      /**
       * Make copies of all vertices that original graph has omitting all arcs.
       * @return List with copies of vertices without arcs.
       */
      public ArrayList<Vertex> getQ() {
         ArrayList<Vertex> res = new ArrayList<>();
         Vertex v = first;
         while (v != null) {
            if (!(res.contains(v))) {
               res.add(new Vertex(v.id, null, null));
               v = v.next;
            }
         }
         for (int i = 0; i < res.size() - 1; i++) {
               res.get(i).next = res.get(i + 1);
         }
         return res;
      }
   }
} 
